//
//  Bitrise1VC.swift
//  BitriseCICDTest
//
//  Created by George Fragkiadakhs on 22/10/2019.
//  Copyright © 2019 George Fragkiadakhs. All rights reserved.
//

import UIKit

class Bitrise1VC: UIViewController {

    @IBOutlet weak var apiUrlLabel: UILabel!
    @IBOutlet weak var apiKeyLabel: UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.apiUrlLabel.text = Environment.rootURL.absoluteString

        self.apiKeyLabel.text = Environment.apiKey
    }
}
